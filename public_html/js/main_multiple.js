var appid = '15d080a77efe4ca06a3a0deb44bbc41a';

function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}
function isEmpty(el) {
    return !$.trim(el.html())
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function getDateFromTimestamp(unix_timestamp) {
    var date = new Date(unix_timestamp * 1000);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime;
}

document.getElementById("btn_add").addEventListener('click', function () {
    var obecne_ciastka = readCookie("miasta");
    console.log(obecne_ciastka);

    if (obecne_ciastka == "") {
        obecne_ciastka += $('#city').val();
    } else {
        obecne_ciastka += ":" + $('#city').val();
    }
    console.log(obecne_ciastka);

    createCookie("miasta", obecne_ciastka, 7);
    refreshCities();
});

function refreshCities() {
    $("#cities").empty();
    var obecne_ciastka = readCookie("miasta");

    var miasta = obecne_ciastka.split(":");
    obecne_ciastka = obecne_ciastka.replace("::", ":");
    for (var i = 0; i < miasta.length; i++) {
        if (miasta[i].trim() == "") {
            continue;
        }

        $("#cities").append("<p><a style=\"width:200px\" class=\"btn btn-default\" >" + miasta[i] + "</a><a class=\"btn btn-default\" href=\"#\" role=\"button\" id=\"btn_del_" + miasta[i] + "\">Usuń</a></p>");
        $("#btn_del_" + miasta[i]).on('click', function () {
            var obecne_ciastka = readCookie("miasta");
            var result = obecne_ciastka.replace("button_del_nazwamiasta".replace("btn_del_", ""), "");
            var result = obecne_ciastka.replace(this.id.replace("btn_del_", ""), "");
            result = result.replace("::", ":");
            createCookie("miasta", result, 7);
            refreshCities();
        });
    }
}
var counter = 0;
function refreshWeather() {
//    $("#cities_weather").empty();
    var container = $("#cities_weather");
    var obecne_ciastka = readCookie("miasta");

    var temp = document.getElementById('temp_type').checked;
    var sign = "&#8451;";
    if (temp) {
        temp = 'imperial';
        sign = "&#8457;";
    } else {
        temp = 'metric';
        sign = "&#8451;";
    }
    var miasta = obecne_ciastka.split(":");
    for (var i = 0; i < miasta.length; i++) {
        if (miasta[i].trim() == "") {
            continue;
        }
        if (isEmpty($("#miasto_" + miasta[i]))) {
            container.append("<div id=\"licznik\" > </div>");
            container.append("<div id=\"miasto_" + miasta[i] + "\"></div>");
        }


        // odrysowanie pogody
        $.ajax({
            url: "http://api.openweathermap.org/data/2.5/weather?q=" + miasta[i] + "&appid=" + appid + "&units=" + temp,
            type: 'GET',
            dataType: 'jsonp',
            success: function (data) { // wywołanie się powiodło
                console.log(data);
                var containerOfCity = $("#miasto_" + data.name);
                console.log(containerOfCity);
                if (isEmpty(containerOfCity)) {
                    containerOfCity.append("<p>Miejsce: " + data.name + ", " + data.sys.country + " - Szerokość geo.:" + data.coord.lat + ", Długość geo.:" + data.coord.lon + " </p>");
                    containerOfCity.append("Zachmurzenie: <div style=\"display:inline;\" id=\"" + data.name + "_clouds\">" + data.clouds.all + "</div>% ");
                    containerOfCity.append("Godzina aktualizacji: " + getDateFromTimestamp(data.dt) + "");
                    containerOfCity.append("<div class='main_data'>");
                    containerOfCity.append("Wilgotność: <div style=\"display:inline;\" id=\"" + data.name + "_humidity\">" + data.main.humidity + "</div>%</br>");
                    containerOfCity.append("Ciśnienie: <div style=\"display:inline;\" id=\"" + data.name + "_pressure\">" + data.main.pressure + "</div></br>");
                    containerOfCity.append("Temperatura: <div style=\"display:inline;\" id=\"" + data.name + "_temp\">" + data.main.temp + " " + sign + "</div> </br>");
                    containerOfCity.append("Temperatura(min): <div style=\"display:inline;\" id=\"" + data.name + "_temp_min\">" + data.main.temp_min + " " + sign + "</div>  </br>");
                    containerOfCity.append("Temperatura(max): <div style=\"display:inline;\" id=\"" + data.name + "_temp_max\">" + data.main.temp_max + " " + sign + "</div>  </br>");
                    containerOfCity.append("Widoczność(w metrach): <div style=\"display:inline;\" id=\"" + data.name + "_visibility\">" + data.visibility + "</div></br>");
                    containerOfCity.append("Wschód słońca: <div style=\"display:inline;\" id=\"" + data.name + "_sunrise\">" + getDateFromTimestamp(data.sys.sunrise) + "</div> </br>");
                    containerOfCity.append("Zachód słońca: <div style=\"display:inline;\" id=\"" + data.name + "_sunset\">" + getDateFromTimestamp(data.sys.sunset) + "</div> </br>");
                    containerOfCity.append("Prędkość wiatru: <div style=\"display:inline;\" id=\"" + data.name + "_wind_speed\">" + data.wind.speed + "</div></br>");
                    containerOfCity.append("Kierunek wiatru: <div style=\"display:inline;\" id=\"" + data.name + "_wind_deg\">" + data.wind.deg + "</div></br>");
//                    containerOfCity.append("Opady deszczu: <div style=\"display:inline;\" id=\"" + data.name + "_rain\">" + data.rain["3h"] + "</div></br>");
//                    containerOfCity.append("Opady śniegu: <div style=\"display:inline;\" id=\"" + data.name + "_snow\">" + data.snow["3h"] + "</div></br>");
                    containerOfCity.append("</div><hr>");
                } else {
                    $("#" + data.name + "_clouds").empty().append("" + data.clouds.all);
                    $("#" + data.name + "_humidity").empty().append("" + data.main.humidity);
                    $("#" + data.name + "_pressure").empty().append("" + data.main.pressure);
                    $("#" + data.name + "_temp").empty().append("" + data.main.temp + " " + sign);
                    $("#" + data.name + "_temp_min").empty().append("" + data.main.temp_min + " " + sign);
                    $("#" + data.name + "_temp_max").empty().append("" + data.main.temp_max + " " + sign);
                    $("#" + data.name + "_visibility").empty().append("" + data.visibility);
                    $("#" + data.name + "_wind_speed").empty().append("" + data.wind.speed);
                    $("#" + data.name + "_wind_deg").empty().append("" + data.wind.deg);
//                    $("#" + data.name + "_rain").empty().append("" + data.rain["3h"]);
//                    $("#" + data.name + "_snow").empty().append("" + data.snow["3h"]);
                    $("#" + data.name + "_sunrise").empty().append("" + getDateFromTimestamp(data.sys.sunrise));
                    $("#" + data.name + "_sunset").empty().append("" + getDateFromTimestamp(data.sys.sunset));
                }
            },
            error: function () {
                containerOfCity.empty().append("Błąd zapytania!");
            }
        });
    }
    $("#licznik").empty().append("" + counter++);
}

setInterval(refreshWeather, 3000);
refreshWeather();
refreshCities();
